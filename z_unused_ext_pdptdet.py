"""
  scrapes boptrad to /files/ext_arkona_boptrad.csv
  6/5/15 added where clause to eliminate some useless bogus data
"""
import pyodbc
import csv
import time
import psycopg2

t1 = time.time()
print t1
pgCon = psycopg2.connect("host='10.130.196.173' dbname='cartiva' user='rydell' password='cartiva'")
pgCursor = pgCon.cursor()
date_cursor = pgCon.cursor()
# pgCursor.execute("truncate dds.ext_pdptdet")

db2Con = pyodbc.connect('DSN=Arkona64; UID=rydejon; PWD=fuckyou5')
db2Cursor = db2Con.cursor()
# file_name = 'files/ext_pdptdet.csv'
# t1 = time.time()
# date_sql = """
#     select *
#     from (
#       select 20100101 as the_date
#       union
#       select 20100102
#       union
#       select 20100103) a
#     order by the_date;
# """

date_sql = """
    select the_date
    from (
      select *, the_year * 10000 + the_month * 100 + the_day as the_date
      from (
      select * from generate_series(2010, 2016, 1) as the_year) a
      cross join (
      select * from generate_series(1, 12, 1) as the_month) b
      cross join (
      select * from generate_series(1, 31, 1) as the_day) c) d
    where d.the_year = 2010
      and d.the_month = 1
    order by the_year,the_month,the_day;
"""
date_cursor.execute(date_sql)
for dt_record in date_cursor:
    db2_sql = """
    select TRIM(PTCO#),TRIM(PTINV#),PTLINE,PTSEQ#,TRIM(PTTGRP),
      TRIM(PTCODE),TRIM(PTSOEP),PTDATE,PTCDATE,TRIM(PTCPID),
      TRIM(PTMANF),TRIM(PTPART),TRIM(PTSGRP),PTQTY,PTCOST,
      PTLIST,PTNET,PTEPCDIFF,TRIM(PTSPCD),TRIM(PTORSO),
      TRIM(PTPOVR),TRIM(PTGPRC),TRIM(PTXCLD),TRIM(PTFPRT),TRIM(PTRTRN),
      PTOHAT,TRIM(PTVATCODE),PTVATAMT,PTTIME,PTQOHCHG,
      PTLASTCHG,TRIM(PTUPDUSER),PTIDENTITY,PTKTXAMT,TRIM(PTLNTAXE),
      TRIM(PTDUPDATE),TRIM(PTRIM)
    FROM  rydedata.pdptdet a
    where ptdate =  """ + str(dt_record[0])
    db2Cursor.execute(db2_sql)
    for record in db2Cursor:
        sql = """
            insert into dds.ext_pdptdet (PTCO_,PTINV_,PTLINE,PTSEQ_,PTTGRP,
                PTCODE,PTSOEP,PTDATE,PTCDATE,PTCPID,
                PTMANF,PTPART,PTSGRP,PTQTY,PTCOST,
                PTLIST,PTNET,PTEPCDIFF,PTSPCD,PTORSO,
                PTPOVR,PTGPRC,PTXCLD,PTFPRT,PTRTRN,
                PTOHAT,PTVATCODE,PTVATAMT,PTTIME,PTQOHCHG,
                PTLASTCHG,PTUPDUSER,PTIDENTITY,PTKTXAMT,PTLNTAXE,
                PTDUPDATE,PTRIM)
            values('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s',
                '%s','%s','%s','%s','%s','%s','%s','%s','%s','%s',
                '%s','%s','%s','%s','%s','%s','%s','%s','%s','%s',
                '%s','%s','%s','%s','%s','%s','%s')
        """ % (str(record[0]), str(record[1]), str(record[2]), str(record[3]), str(record[4]),
               str(record[5]), str(record[6]), str(record[7]), str(record[8]), str(record[9]),
               str(record[10]), str(record[11]), str(record[12]), str(record[13]), str(record[14]),
               str(record[15]), str(record[16]), str(record[17]), str(record[18]), str(record[19]),
               str(record[20]), str(record[21]), str(record[22]), str(record[23]), str(record[24]),
               str(record[25]), str(record[26]), str(record[27]), str(record[28]), str(record[29]),
               str(record[30]), str(record[31]), str(record[32]), str(record[33]), str(record[34]),
               str(record[35]), str(record[36]))
        # print sql
        pgCursor.execute(sql)
        pgCon.commit()

# db2Cursor.execute(sql)

# with open(file_name, 'wb') as f:
#     csv.writer(f).writerows(db2Cursor)

t2 = time.time()
print t2 - t1
db2Cursor.close()
db2Con.close()
# f.close()

# with open(file_name, 'r') as io:
#     pgCursor.copy_expert("""copy dds.ext_pdptdet from stdin with csv encoding 'latin-1) '""", io)
# pgCon.commit()


pgCursor.close()
pgCon.close()
