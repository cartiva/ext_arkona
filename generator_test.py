# encoding=utf-8

import db_cnx
import csv
from datetime import datetime

pg_con = None
db2_con = None
start_time = datetime.now()
file_name = 'files/ext_bopvref.csv'
print start_time
try:
    with db_cnx.arkona_report() as db2_con:
        with db2_con.cursor() as db2_cur:
            sql = """
              SELECT TRIM(COMPANY_NUMBER),CUSTOMER_KEY,TRIM(VIN),START_DATE,END_DATE,TRIM(TYPE_CODE),
                TRIM(SALESPERSON),TRIM(DEALER_NUMBER),TRIM(LIEN_HOLDER)
              from rydedata.bopvref
              -- where start_date = 20170518
            """
            db2_cur.execute(sql)
            with open(file_name, 'wb') as f:
                csv.writer(f).writerows(db2_cur)
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            pg_cur.execute("truncate arkona.ext_bopvref")
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy arkona.ext_bopvref from stdin with csv encoding 'latin-1 '""", io)
    # def iter_row(cursor, size):
    #     while True:
    #         rows = cursor.fetchmany(size)
    #         if not rows:
    #             break
    #         for row in rows:
    #             yield row
    #
    # with db_cnx.pg() as pg_con:
    #     with pg_con.cursor() as pg_cur:
    #         pg_cur.execute("truncate arkona.ext_bopvref")
    #         with db_cnx.arkona_report() as db2_con:
    #             with db2_con.cursor() as db2_cur:
    #                 sql = """
    #                   SELECT TRIM(COMPANY_NUMBER),CUSTOMER_KEY,TRIM(VIN),START_DATE,END_DATE,TRIM(TYPE_CODE),
    #                     TRIM(SALESPERSON),TRIM(DEALER_NUMBER),TRIM(LIEN_HOLDER)
    #                   from rydedata.bopvref
    #                   -- where start_date > 20170000
    #                 """
    #                 db2_cur.execute(sql)
    #                 for row in iter_row(db2_cur, 1024):
    #                     # print row
    #                     sql = """
    #                         insert into arkona.ext_bopvref values ('{0}',{1},'{2}',{3},{4},'{5}','{6}','{7}','{8}')
    #                     """.format(*row)
    #                     pg_cur.execute(sql)
except Exception, error:
    print error
finally:
    print datetime.now() - start_time
    if db2_con:
        db2_con.close()
    if pg_con:
        pg_con.close()
