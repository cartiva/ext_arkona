import pyodbc
import csv
import time
import psycopg2
# import psycopg2.extras

# 1/3/15 systables fails at csv.writer if table_text is in the query
db2Con = pyodbc.connect('DSN=Arkona64; UID=rydejon; PWD=fuckyou5')
db2Cursor = db2Con.cursor()
out_file = 'files/arkona_columns.csv'
t1 = time.time()
db2Cursor.execute("""
select a.table_name, a.system_column_name, a.column_name, a.ordinal_position,
  a.data_type, a.length, a.numeric_scale, a.column_text
from  qsys2.syscolumns a
where a.table_schema = 'rydedata'
  and trim(a.table_name) <> 'glpayware'
  and a.table_name not like '$%'
  and a.table_name not like 'x%'
""")

# db2Cursor.execute("""
# select a.table_name, a.column_count, a.row_length, table_owner, table_text
# FROM  qsys2.systables a
# WHERE a.table_schema = 'RYDEDATA'
#   and trim(a.table_name) <> 'GLPAYWARE'
#   and a.table_name not like '$%'
#   and a.table_name not like 'X%'""")

with open(out_file, 'wb') as f:
    csv.writer(f).writerows(db2Cursor)
db2Cursor.close()
db2Con.close()
f.close()

pgCon = psycopg2.connect("host='172.17.196.73' dbname='Cartiva' user='postgres' password='cartiva'")
pgCursor = pgCon.cursor()
pgCursor.execute("truncate test.arkona_syscolumns")
pgCon.commit()

io = open(out_file, 'r')

pgCursor.copy_expert("""copy test.arkona_syscolumns from stdin with (format csv)""", io)

pgCon.commit()
io.close()

t2 = time.time()
print t2 - t1

io.close()
pgCursor.close()
pgCon.close()
