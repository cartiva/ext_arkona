# encoding=utf-8
import csv
import db_cnx
import ops
import string

task = 'ext_inpmast'
pg_con = None
db2_con = None
run_id = None
file_name = 'files/ext_inpmast.csv'

try:
    run_id = ops.log_start(task)
    if ops.dependency_check(task) != 0:
        ops.log_dependency_failure(run_id)
        print 'Failed dependency check'
        exit()
    with db_cnx.arkona_report() as db2_con:
        with db2_con.cursor() as db2_cur:
            sql = """
                SELECT TRIM(INPMAST_COMPANY_NUMBER),TRIM(INPMAST_VIN),TRIM(VIN_LAST_6),TRIM(INPMAST_STOCK_NUMBER),
                  TRIM(INPMAST_DOCUMENT_NUMBER),TRIM(STATUS),TRIM(G_L_APPLIED),TRIM(TYPE_N_U),
                  TRIM(BUS_OFF_FRAN_CODE),TRIM(SERVICE_FRAN_CODE),TRIM(MANUFACTURER_CODE),
                  TRIM(VEHICLE_CODE),YEAR,TRIM(MAKE),TRIM(MODEL_CODE),TRIM(MODEL),TRIM(BODY_STYLE),
                  TRIM(COLOR),TRIM(TRIM),TRIM(FUEL_TYPE),TRIM(MPG),CYLINDERS,TRIM(TRUCK),TRIM(WHEEL_DRIVE4WD),
                  TRIM(TURBO),TRIM(COLOR_CODE),TRIM(ENGINE_CODE),TRIM(TRANSMISSION_CODE),TRIM(IGNITION_KEY_CODE),
                  TRIM(TRUNK_KEY_CODE),TRIM(KEYLESS_CODE),TRIM(RADIO_CODE),TRIM(WHEEL_LOCK_CODE),
                  TRIM(DEALER_CODE),TRIM(LOCATION),ODOMETER,DATE_IN_INVENT,DATE_IN_SERVICE,DATE_DELIVERED,
                  DATE_ORDERED,TRIM(INPMAST_SALE_ACCOUNT),TRIM(INVENTORY_ACCOUNT),TRIM(DEMO_NAME),
                  WARRANTY_MONTHS,WARRANTY_MILES,WARRANTY_DEDUCT,LIST_PRICE,INPMAST_VEHICLE_COST,
                  TRIM(OPTION_PACKAGE),TRIM(LICENSE_NUMBER),GROSS_WEIGHT,WORK_IN_PROCESS,INSPECTION_MONTH,
                  TRIM(ODOMETER_ACTUAL),BOPNAME_KEY,TRIM(KEY_TO_CAP_EXPLOSION_DATA),TRIM(CO2_EMISSION_CODE2),
                  REGISTRATION_DATE1,FUNDING_EXPIRATION_DATE2,INSPECTION_DATE3,TRIM(DRIVERS_SIDE),
                  FREE_FLOORING_PERIOD,TRIM(ORDERED_STATUS),TRIM(PUBLISH_VEHICLE_INFO_TO_WEB),TRIM(SALE),
                  TRIM(CERTIFIED_USED_CAR),LAST_SERVICE_DATE4,NEXT_SERVICE_DATE5,DEALER_CODE_IMDLRCD,
                  COMMON_VEHICLE_ID,CHROME_STYLE_ID,CREATED_USER_CODE,UPDATED_USER_CODE,CREATED_TIMESTAMP,
                  UPDATED_TIMESTAMP,TRIM(STOCKED_COMPANY),ENGINE_HOURS
                FROM  rydedata.inpmast a
            """
            db2_cur.execute(sql)
            with open(file_name, 'wb') as f:
                csv.writer(f).writerows(db2_cur)
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            pg_cur.execute("truncate dds.ext_inpmast")
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy dds.ext_inpmast from stdin with csv encoding 'latin-1 '""", io)
    ops.log_pass(run_id)
    print 'Passsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss'
except Exception, error:
    ops.log_error(str(run_id), string.replace(str(error), "'", ""))
    print error
finally:
    if db2_con:
        db2_con.close()
    if pg_con:
        pg_con.close()
