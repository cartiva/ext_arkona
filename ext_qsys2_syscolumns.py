# encoding=utf-8
import csv
import db_cnx
import ops
import string

task = 'ext_qsys2_syscolumns'
pg_con = None
db2_con = None
run_id = None
file_name = 'files/ext_qsys2_syscolumns.csv'

try:
    run_id = ops.log_start(task)
    if ops.dependency_check(task) != 0:
        ops.log_dependency_failure(run_id)
        print 'Failed dependency check'
        exit()
    with db_cnx.arkona_report() as db2_con:
        with db2_con.cursor() as db2_cur:
            sql = """
                select a.table_name, a.system_column_name, a.column_name, a.ordinal_position,
                  a.data_type, a.length, a.numeric_scale, a.column_text
                from  qsys2.syscolumns a
                where a.table_schema = 'RYDEDATA'
                  and trim(a.table_name) <> 'GLPAYWARE'
                  and a.table_name not like '$%'
                  and a.table_name not like 'X%'
            """
            db2_cur.execute(sql)
            with open(file_name, 'wb') as f:
                csv.writer(f).writerows(db2_cur)
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            pg_cur.execute("truncate dds.ext_qsys2_syscolumns")
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy dds.ext_qsys2_syscolumns from stdin with csv encoding 'latin-1 '""", io)
    ops.log_pass(run_id)
    print 'Passsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss'
except Exception, error:
    ops.log_error(str(run_id), string.replace(str(error), "'", ""))
    print error
finally:
    if db2_con:
        db2_con.close()
    if pg_con:
        pg_con.close()
