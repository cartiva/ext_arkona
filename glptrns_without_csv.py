from __future__ import generators  # needs to be at the top of your module
import pyodbc
import psycopg2
import datetime

# fails on strings that include ':   'KETTERLING'S JUNK YARD 1102-08'
# def ResultIter(cursor, arraysize=500):
#     """An iterator that uses fetchmany to keep memory usage down"""
#     while True:
#         results = cursor.fetchmany(arraysize)
#         if not results:
#             break
#         for result in results:
#             yield result


print ('start:  ' + datetime.datetime.now().time().strftime("%H:%M:%S.%f"))
start_time = datetime.datetime.now().time().strftime("%H:%M:%S.%f")
table_name = 'test.ext_glptrns_1130'

db2Con = pyodbc.connect('DSN=Arkona64; UID=rydejon; PWD=fuckyou5')
db2Cursor = db2Con.cursor()

pgCon = psycopg2.connect("host='172.17.196.73' dbname='Cartiva' user='postgres' password='cartiva'")
pgCursor = pgCon.cursor()
pgCursor.execute("truncate " + table_name)

db2Cursor.execute("""
    select
      gtco# as gtco, gttrn# as gttrn, gtseq# as gtseq, gtdtyp, gttype, gtpost, gtrsts, gtadjust, gtpsel,
      gtjrnl, gtdate, gtrdate, gtsdate, trim(gtacct) as gtacct, trim(gtctl#) as gtctl, trim(gtdoc#) as gtdoc,
      trim(gtrdoc#) as gtrdoc, gtrdtyp, trim(gtodoc#) as gtodoc, trim(gtref#) as gtref, trim(gtvnd#) as gtvnd,
      trim(gtdesc) as gtdesc, gttamt, gtcost, gtctlo, gtoco# as gtoco
    FROM  rydedata.glptrns
    where gtdate between '2015-11-01' and current_date
""")
# for row in ResultIter(db2Cursor):
for row in db2Cursor:
    sql = """
        insert into """ + table_name + """ (gtco_, gttrn_, gtseq_, gtdtyp, gttype, gtpost, gtrsts, gtadjust,
        gtpsel,  gtjrnl, gtdate, gtrdate, gtsdate, gtacct, gtctl_, gtdoc_,  gtrdoc_, gtrdtyp, gtodoc_, gtref_, gtvnd_,
        gtdesc, gttamt, gtcost, gtctlo, gtoco_)
        values('%s','%s', '%s','%s','%s', '%s','%s','%s', '%s','%s','%s', '%s','%s','%s', '%s','%s','%s', '%s',
        '%s','%s', '%s','%s','%s', '%s','%s','%s')
    """ % (row.GTCO, row.GTTRN, row.GTSEQ, row.GTDTYP, row.GTTYPE, row.GTPOST, row.GTRSTS, row.GTADJUST, row.GTPSEL,
           row.GTJRNL, row.GTDATE, row.GTRDATE, row.GTSDATE, row.GTACCT, row.GTCTL, row.GTDOC,
           row.GTRDOC, row.GTRDTYP, row.GTODOC, row.GTREF, row.GTVND,
           row.GTDESC.replace("'", "''"), row.GTTAMT, row.GTCOST, row.GTCTLO, row.GTOCO)
    # print row
    # print row.GTDESC.replace("'","''")
    pgCursor.execute(sql)
    for i, v in enumerate(db2Cursor):
        'do nothing'
    # print row
    # print str(row)
    # pgCursor.execute(""" insert into test.ext_glptrns_1130 values """ + str(row))
    # # print row[1]
    # # print row.GTCO
    pgCon.commit()

pgCursor.close()
pgCon.close()
db2Cursor.close()
db2Con.close()
print ('start:  ' + start_time)
print ('finish:  ' + datetime.datetime.now().time().strftime("%H:%M:%S.%f"))
