# scrapes glptrns to /files/ext_arkona_boptrad.csv
# populates production db(173) schema greg table ext_arkona_glptrns
# 22 sec for one day
# 370 secs for one month
# 2015 (7 months)
# 8/1/15 damnit, just do the trim in the initial db2 query
# 11/27/15: CDC

import pyodbc
import csv
import psycopg2
import datetime
# print ('start:  ' + datetime.datetime.now().time().strftime("%H:%M:%S.%f"))
# start_time = datetime.datetime.now().time().strftime("%H:%M:%S.%f")
table_name = 'test.ext_glptrns_1203'
db2Con = pyodbc.connect('DSN=Arkona64; UID=rydejon; PWD=fuckyou5')
db2Cursor = db2Con.cursor()
out_file = 'files/ext_arkona_glptrns_tmp.csv'
db2Cursor.execute("""
select
  gtco#, gttrn#, gtseq#, gtdtyp, gttype, gtpost, gtrsts, gtadjust, gtpsel,
  gtjrnl, gtdate, gtrdate, gtsdate, trim(gtacct), trim(gtctl#), trim(gtdoc#),
  trim(gtrdoc#), gtrdtyp, trim(gtodoc#), trim(gtref#), trim(gtvnd#),
  trim(gtdesc), gttamt, gtcost, gtctlo,gtoco#
FROM  rydedata.glptrns
where gtdate between '2015-11-01' and current_date
""")
with open(out_file, 'wb') as f:
    csv.writer(f).writerows(db2Cursor)
db2Cursor.close()
db2Con.close()
pgCon = psycopg2.connect("host='172.17.196.73' dbname='Cartiva' user='postgres' password='cartiva'")
pgCursor = pgCon.cursor()
pgCursor.execute("truncate " + table_name)
pgCon.commit()
with open(out_file, 'r') as io:
    pgCursor.copy_expert("""copy """ + table_name + """  from stdin with csv encoding 'latin-1) '""", io)
pgCon.commit()

pgCursor.close()
pgCon.close()

# print ('start:  ' + start_time)
# print ('finish:  ' + datetime.datetime.now().time().strftime("%H:%M:%S.%f"))
