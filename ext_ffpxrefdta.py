# encoding=utf-8
import csv
import db_cnx
import ops
import string

task = 'ext_ffpxrefdta'
pg_con = None
db2_con = None
run_id = None
file_name = 'files/ext_ffpxrefdta.csv'

try:
    run_id = ops.log_start(task)
    if ops.dependency_check(task) != 0:
        ops.log_dependency_failure(run_id)
        print 'Failed dependency check'
        exit()
    with db_cnx.arkona_report() as db2_con:
        with db2_con.cursor() as db2_cur:
            sql = """
                select TRIM(COMPANY_NUMBER),TRIM(CONSOLIDATION_GRP),TRIM(G_L_COMPANY_),TRIM(FACTORY_CODE),
                  FACTORY_FINANCIAL_YEAR,TRIM(G_L_ACCT_NUMBER),TRIM(FACTORY_ACCOUNT),TRIM(FACTORY_ACCOUNT2),
                  TRIM(FACTORY_ACCOUNT3),TRIM(FACTORY_ACCOUNT4),TRIM(FACTORY_ACCOUNT5),TRIM(FACTORY_ACCOUNT6),
                  TRIM(FACTORY_ACCOUNT7),TRIM(FACTORY_ACCOUNT8),TRIM(FACTORY_ACCOUNT9),TRIM(FACTORY_ACCOUNT10),
                  FACT_ACCOUNT_,FACT_ACCOUNT__FXFP02,FACT_ACCOUNT__FXFP03,FACT_ACCOUNT__FXFP04,
                  FACT_ACCOUNT__FXFP05,FACT_ACCOUNT__FXFP06,FACT_ACCOUNT__FXFP07,FACT_ACCOUNT__FXFP08,
                  FACT_ACCOUNT__FXFP09,FACT_ACCOUNT__FXFP10
                from rydedata.ffpxrefdta
            """
            db2_cur.execute(sql)
            with open(file_name, 'wb') as f:
                csv.writer(f).writerows(db2_cur)
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            pg_cur.execute("truncate dds.ext_ffpxrefdta")
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy dds.ext_ffpxrefdta from stdin with csv encoding 'latin-1 '""", io)
    ops.log_pass(run_id)
    print 'Passsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss'
except Exception, error:
    ops.log_error(str(run_id), string.replace(str(error), "'", ""))
    print error
finally:
    if db2_con:
        db2_con.close()
    if pg_con:
        pg_con.close()
