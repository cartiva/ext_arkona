# encoding=utf-8
import csv
import db_cnx
import ops
import string

task = 'ext_pymast'
pg_con = None
db2_con = None
run_id = None
file_name = 'files/ext_pymast.csv'

try:
    run_id = ops.log_start(task)
    if ops.dependency_check(task) != 0:
        ops.log_dependency_failure(run_id)
        print 'Failed dependency check'
        exit()
    with db_cnx.arkona_report() as db2_con:
        with db2_con.cursor() as db2_cur:
            sql = """
                select
                  TRIM(PYMAST_COMPANY_NUMBER),TRIM(PYMAST_EMPLOYEE_NUMBER),TRIM(ACTIVE_CODE),TRIM(DEPARTMENT_CODE),
                  DEPARTMENT_EXT,SECURITY_GROUP,TRIM(STATE_CODE_UNEMPLOYMENT),TRIM(COUNTY_CODE_UNEMPLOYMENT),
                  TRIM(CITY_CODE_UNEMPLOYMENT),TRIM(EMPLOYEE_NAME),RECORD_KEY,TRIM(ADDRESS_1),TRIM(ADDRESS_2),
                  TRIM(CITY_NAME),
                  TRIM(STATE_CODE_ADDRESS_),ZIP_CODE,TEL_AREA_CODE,TELEPHONE_NUMBER,SOC_SEC_NUMBER,
                  TRIM(DRIVERS_LICENSE_),BIRTH_DATE,HIRE_DATE,ORG_HIRE_DATE,LAST_RAISE_DATE,TERMINATION_DATE,
                  TRIM(SEX),TRIM(FULL_TIME_PART_TIME),TRIM(MARITAL_STATUS),TRIM(EXEMPT_FROM_FICA),
                  TRIM(EXEMPT_FROM_FED),FEDERAL_EXEMPTIONS,FEDERAL_ADD_ON_AMOUNT,FEDERAL_ADD_ON_PERCENT,
                  TRIM(BENFIT_TYPE),TRIM(PAYROLL_CLASS),TRIM(EIC_ELIGABLE),TRIM(EIC_CLASS),TRIM(PAY_PERIOD),
                  BASE_SALARY,BASE_HRLY_RATE,ALT_SALARY,ALT_HRLY_RATE,TRIM(DISTRIB_CODE),TRIM(ELIG_FOR_RETIRE),
                  RETIREMENT_,RETIREMENT_FIXED_AMOUNT,RETIREMENT_ANNUAL_MAX,WKMAN_COMP_CODE,TRIM(CUM_WAGE_WITHHLD),
                  TRIM(STATE_TAX_TAB_CODE),TRIM(COUNTY_TAX_TAB_CODE),TRIM(CITY_TAX_TAB_CODE),LAST_UPDATE,LAST_CHECK,
                  TRIM(DECEASED_FLAG),TRIM(EXEMPT_FROM_STATE),STATE_EXEMPTIONS,STATE_TAX_ADD_ON_AMOUNT,
                  STATE_ADD_ON_PERCENT,STATE_FIXED_EXEMPTIONS,TRIM(EXEMPT_FROM_COUNTY),COUNTY_TAX_ADD_ON_AMOUNT,
                  COUNTY_ADD_ON_PERCENT,TRIM(EXEMPT_FROM_LOCAL),LOCAL_TAX_ADD_ON_AMOUNT,LOCAL_ADD_ON_PERCENT,
                  TRIM(BANK_INFO),DEFERRED_VACATION,DEFERRED_HOLIDAYS,DEFFERED_SICKLEAVE,DEFERRED_VAC_YEAR,
                  DEFERRED_HOL_YEAR,DEFFERED_SIC_YEAR,VAC_HRLY_RATE,HOL_HRLY_RATE,SCK_HRLY_RATE,TRIM(A_R_CUSTOMER_),
                  TRIM(DRIVER_LIC_STATE_CODE),DRIVER_LIC_EXPIRE_DATE,TRIM(FEDERAL_FILING_STATUS),
                  TRIM(STATE_FILING_STATUS),TRIM(COUNTY_FILING_STATUS),TRIM(CITY_FILING_STATUS),
                  TRIM(STATE_FILING_STATUS_2),TRIM(COUNTY_FILING_STATUS_2),TRIM(CITY_FILING_STATUS_2),
                  TRIM(STATE_TAX_TAB_CODE2),TRIM(COUNTY_TAX_TAB_CODE2),TRIM(CITY_TAX_TAB_CODE2),TRIM(SDI_STATE),
                  TRIM(WRKR_COMP_STATE),TRIM(REPORTING_CODE_1),TRIM(REPORTING_CODE_2),TRIM(EEOC),TRIM(OVERTIME_EXEMPT),
                  TRIM(HIGH_COMPENSATE_FLAG),K_ELIGIBILITY_DATE01E,K_MATCHING_DATE01M,
                  TRIM(TEMP_ADD_1),TRIM(TEMP_ADD_2),TRIM(TEMP_ADD_3),TRIM(TEMP_ADD_4),
                  TRIM(TEMP_ADD_5),TRIM(TEMP_ADD_6),TRIM(TEMP_ADD_7),TEMP_ADD_9,TEMP_ADD10
                from rydedata.pymast
            """
            db2_cur.execute(sql)
            with open(file_name, 'wb') as f:
                csv.writer(f).writerows(db2_cur)
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            pg_cur.execute("truncate arkona.ext_pymast")
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy arkona.ext_pymast from stdin with csv encoding 'latin-1 '""", io)
    ops.log_pass(run_id)
    print 'Passsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss'
except Exception, error:
    ops.log_error(str(run_id), string.replace(str(error), "'", ""))
    print error
finally:
    if db2_con:
        db2_con.close()
    if pg_con:
        pg_con.close()
