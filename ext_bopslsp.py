# encoding=utf-8
import csv
import db_cnx
import ops
import string

task = 'ext_bopslsp'
pg_con = None
db2_con = None
run_id = None
file_name = 'files/ext_bopslsp.csv'

try:
    run_id = ops.log_start(task)
    if ops.dependency_check(task) != 0:
        ops.log_dependency_failure(run_id)
        print 'Failed dependency check'
        exit()
    with db_cnx.arkona_report() as db2_con:
        with db2_con.cursor() as db2_cur:
            sql = """
              SELECT TRIM(COMPANY_NUMBER),TRIM(SALES_PERSON_ID),TRIM(SALES_PERSON_NAME),TRIM(SALES_PERSON_TYPE),
              TRIM(ACTIVE),TRIM(EMPLOYEE_NUMBER),TRIM(TEAM),GROSS_COMM_RATE,PRICE_COMM_RATE,NET_P_COMM_RATE,
              HLDBCK_COMM_RATE,DOC_FEE_COMM_RATE,RESERVE_COMM_RATE,INSUR_COMM_RATE,SERV_CO_COMM_RATE,
              AMO_COMM_RATE,INCENT_COMM_RATE,GROSS_BONUS,PRICE_BONUS,NET_P_BONUS,HLDBCK_BONUS,DOC_FEE_BONUS,
              RESERVE_BONUS,INSURANCE_BONUS,SERV_CONT_BONUS,AMO_BONUS,INCENTIVE_BONUS,GROSS_MINIMUM,
              PRICE_MINIMUM,NET_P_MINIMUM,HLDBCK_MINIMUM,DOC_FEE_MINIMUM,RESERVE_MINIMUM,INSUR_MINIMUM,
              SERV_CONT_MINIMUM,AMO_MINIMUM,INCENTIVE_MINIMUM,TRIM(PASSWORD),TRIM(AUTH_APPRAISER),SHOW_ROOM,
              TRIM(G_L_COMPANY),TRIM(STATE_LICENSE_),ACCESS_COMM_RATE,ACCESS_BONUS,ACCESS_MINIMUM,
              ACCESS_MINIMUM_BPACMINO,TRIM(ALTERNAME_S_P_ID)
              from rydedata.bopslsp
            """
            db2_cur.execute(sql)
            with open(file_name, 'wb') as f:
                csv.writer(f).writerows(db2_cur)
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            pg_cur.execute("truncate arkona.ext_bopslsp")
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy arkona.ext_bopslsp from stdin with csv encoding 'latin-1 '""", io)
    ops.log_pass(run_id)
    print 'Passsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss'
except Exception, error:
    ops.log_error(str(run_id), string.replace(str(error), "'", ""))
    print error
finally:
    if db2_con:
        db2_con.close()
    if pg_con:
        pg_con.close()
