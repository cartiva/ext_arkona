# encoding=utf-8
import csv
import db_cnx
import ops
import string

task = 'ext_sdprhdr'
pg_con = None
db2_con = None
run_id = None
file_name = 'files/ext_sdprhdr.csv'

try:
    # run_id = ops.log_start(task)
    # if ops.dependency_check(task) != 0:
    #     ops.log_dependency_failure(run_id)
    #     print 'Failed dependency check'
    #     exit()
    with db_cnx.arkona_report() as db2_con:
        with db2_con.cursor() as db2_cur:
            sql = """
              SELECT {TRIM(COMPANY_NUMBER),TRIM(RO_NUMBER),TRIM(RO_TYPE),WARRANTY_RO_,TRIM(SERVICE_WRITER_ID),
                TRIM(RO_TECHNICIAN_ID),CUSTOMER_KEY,TRIM(CUST_NAME),A_R_CUST_KEY,TRIM(PAYMENT_METHOD),OPEN_DATE,
                CLOSE_DATE,FINAL_CLOSE_DATE,TRIM(VIN),TOTAL_ESTIMATE,SERV_CONT_COMP,SERV_CONT_COMP_2,
                SERV_CONT_DEDUCT,SERV_CONT_DEDUCT2,WARRANTY_DEDUCT,TRIM(FRANCHISE_CODE),ODOMETER_IN,
                ODOMETER_OUT,TRIM(CHECK_NUMBER),TRIM(PURCHASE_ORDER_),RECEIPT_NUMBER,PARTS_TOTAL,LABOR_TOTAL,
                SUBLET_TOTAL,SC_DEDUCT_PAID,SERV_CONT_TOTAL,SPEC_ORD_DEPOS,CUST_PAY_HZRD_MAT,CUST_PAY_SALE_TAX,
                CUST_PAY_SALE_TAX_2,CUST_PAY_SALE_TAX_3,CUST_PAY_SALE_TAX_4,WARRANTY_SALE_TAX,
                WARRANTY_SALE_TAX_2,WARRANTY_SALE_TAX_3,WARRANTY_SALE_TAX_4,INTERNAL_SALE_TAX,
                INTERNAL_SALE_TAX_2,INTERNAL_SALE_TAX_3,INTERNAL_SALE_TAX_4,SVC_CONT_SALE_TAX,
                SVC_CONT_SALE_TAX_2,SVC_CONT_SALE_TAX_3,SVC_CONT_SALE_TAX_4,CUST_PAY_SHOP_SUP,
                WARRANTY_SHOP_SUP,INTERNAL_SHOP_SUP,SVC_CONT_SHOP_SUP,COUPON_NUMBER,COUPON_DISCOUNT,
                TOTAL_COUPON_DISC,TOTAL_SALE,TOTAL_GROSS,TRIM(INTERNAL_AUTH_BY),TRIM(TAG_NUMBER),
                DATE_TIME_OF_APPOINTMENT,DATE_TIME_LAST_LINE_COMPLETE,DATE_TIME_PRE_INVOICED,
                TRIM(TOWED_IN_INDICATOR),DOC_CREATE_TIMESTAMP,TRIM(DISPATCH_TEAM),TAX_GROUP_OVERRIDE,
                DISCOUNT_TAXABLE_AMOUNT,PARTS_DISCOUNT_ALLOCATION_AMOUNT,LABOR_DISCOUNT_ALLOCATION_AMOUNT}
              from rydedata.sdprhdr
              where open_date > 20160000
            """
            db2_cur.execute(sql)
            with open(file_name, 'wb') as f:
                csv.writer(f).writerows(db2_cur)
    with db_cnx.pg() as pg_con:
        with pg_con.cursor() as pg_cur:
            pg_cur.execute("truncate arkona.ext_sdprhdr")
            with open(file_name, 'r') as io:
                pg_cur.copy_expert("""copy arkona.ext_sdprhdr from stdin with csv encoding 'latin-1 '""", io)
    # ops.log_pass(run_id)
    print 'Passsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss'
except Exception, error:
    # ops.log_error(str(run_id), string.replace(str(error), "'", ""))
    print error
finally:
    if db2_con:
        db2_con.close()
    if pg_con:
        pg_con.close()
